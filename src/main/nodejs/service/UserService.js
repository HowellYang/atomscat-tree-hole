let UserDao = require('../dao/UserDao');
let grpc = require('grpc');
let protoLoader = require('@grpc/proto-loader');

let PROTO_PATH = __node_modules_path + 'atomscat-organization-api/src/protos/OrganizationService.proto';
let packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    });
let atomscat = grpc.loadPackageDefinition(packageDefinition).atomscat;
let client = new atomscat.OrganizationService('localhost:50051', grpc.credentials.createInsecure());

class UserService {

    getUserList(callback) {
        let userDao = new UserDao();
        userDao.getUserList(function (list) {
            list.forEach(function (item) {
                delete item['password'];
            });

            callback(list);
        });
    }

    getOrganization(user, callback) {
        client.getOrganizationList({name: user}, function(err, response) {
            callback(response);
            console.log('OrganizationService:', response.message);
        });
    }

    getOrganizationPage(call, callback) {
        client.getOrganizationPage(call, function(err, response) {
            callback(response);
            console.log('getOrganizationPage:', response.message);
        });
    }

}

module.exports = UserService;
