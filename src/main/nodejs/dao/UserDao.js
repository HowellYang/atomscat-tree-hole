let mysql = require('../config/mysql');

class UserDao {
    getUserList(callback) {
        let sqlParamsEntity = [];
        let sql1 = "SELECT * FROM t_user";
        let param1 = {};
        sqlParamsEntity.push(mysql.sqlParamEntity(sql1, param1));
        mysql.execTrans(sqlParamsEntity, function(err, info, result){
            if(err){
                console.error("事务执行失败");
            }else{
                console.info(result[0][0].length);
                callback(result[0][0]);
                console.log("done.");
            }
        });
    }
}
module.exports = UserDao;
