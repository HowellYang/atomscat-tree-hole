let mysql = require('mysql');
let async = require("async");

module.exports = {
    execTrans: execTrans,
    sqlParamEntity: sqlParamEntity
};


function sqlParamEntity(sql, params, callback) {
    return {
        sql: sql,
        params: params,
        callback: callback
    };
}

function execTrans(sqlparamsEntities, callback) {
    let pool = mysql.createPool({
        host: config.mysql.host,
        user: config.mysql.user,
        password: config.mysql.password,
        database: config.mysql.database,
        port: config.mysql.port,
        connectionLimit: 10,
        waitForConnections: false
    });
    pool.getConnection(function (err, connection) {
        if (err) {
            return callback(err, null);
        }
        connection.beginTransaction(function (err) {
            if (err) {
                return callback(err, null);
            }
            console.log("开始执行transaction，共执行" + sqlparamsEntities.length + "条数据");
            let funcAry = [];
            sqlparamsEntities.forEach(function (sql_param) {
                let temp = function (done) {
                    let sql = sql_param.sql;
                    let param = sql_param.params;
                    connection.query(sql, param, function (tErr, rows, fields) {
                        if (tErr) {
                            connection.rollback(function () {
                                console.log("事务失败，" + sql_param + "，ERROR：" + tErr);
                                throw tErr;
                            });
                        } else {
                            // sql_param.callback(tErr, rows, fields);
                            return done(tErr, rows, fields);
                        }
                    })
                };
                funcAry.push(temp);
            });

            async.series(funcAry, function (err, result) {
                console.log("transaction error: " + err);
                if (err) {
                    connection.rollback(function (err) {
                        console.log("transaction error: " + err);
                        connection.release();
                        return callback(err, null);
                    });
                } else {
                    connection.commit(function (err, info) {
                        console.log("transaction info: " + JSON.stringify(info));
                        if (err) {
                            console.log("执行事务失败，" + err);
                            connection.rollback(function (err) {
                                console.log("transaction error: " + err);
                                connection.release();
                                return callback(err, null);
                            });
                        } else {
                            connection.release();
                            return callback(null, info, result);
                        }
                    })
                }
            })
        });
    });
}
