let express = require('express');
let UserService = require('../service/UserService');

let router = express.Router();


/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/', function (req, res, next) {
  try {
    let userService = new UserService();
    // userService.getUserList(function (list) {
    //   res.json(list);
    // });

    userService.getOrganization(req.body.name, function (response) {
      res.json(response);
    })

    // console.info("post: " + JSON.stringify(req.body));
    // res.json(req.body);
  } catch (e) {
    res.json('{"code":"500"}')
  }
});


router.post('/page', function (req, res, next) {
  try {
    let userService = new UserService();
    // userService.getUserList(function (list) {
    //   res.json(list);
    // });

    userService.getOrganizationPage(req.body, function (response) {
      res.json(response);
    })

    // console.info("post: " + JSON.stringify(req.body));
    // res.json(req.body);
  } catch (e) {
    res.json('{"code":"500"}')
  }
});

module.exports = router;
